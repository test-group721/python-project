from flask import Flask, jsonify, render_template
import gitlab
import os
import requests

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False


@app.errorhandler(404)
def all_exceptions(e):
    return jsonify(error=str(e))


@app.errorhandler(requests.exceptions.ConnectTimeout)
def conn_timeout(e):
    return jsonify(error=str(e))


@app.route('/users')
def users():
    token = os.environ.get("api-token")
    webaddr = os.environ.get("web-addr")
    groupid = os.environ.get("group-id")

    try:
        gl = gitlab.Gitlab(webaddr, private_token=token, timeout=5)
    except:
        return ('There is some error')
    else:
        try:
            group = gl.groups.get(groupid)
        except gitlab.exceptions.GitlabGetError as err:
            return jsonify(error=str(err))
        else:
            members = group.members.list()
            for member in members:
                return jsonify(id=member.id,name=member.name)


@app.route('/health')
def health():
    webaddr = os.environ.get("web-addr")
    token = os.environ.get("api-token-all")
    projectid = os.environ.get("project-id")
    headers = {'PRIVATE-TOKEN': token}
    url = webaddr + '/api/v4/projects/' + projectid

    try:
        gl = gitlab.Gitlab(webaddr, private_token=token)
    except:
        return ('There is some error')
    else:
        response = requests.get(url, headers=headers, timeout=5)
        if response.status_code == 401:
            return "Token Error"
        elif response.status_code == 200:
            return jsonify(response = response.status_code)
        else:
            return jsonify({"response":200})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port="8000", debug=False)
